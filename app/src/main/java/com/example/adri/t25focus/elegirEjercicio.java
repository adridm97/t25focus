package com.example.adri.t25focus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class elegirEjercicio extends AppCompatActivity {

    private Button btnCardio;
    private Button btnAbs;
    private Button btnEjerTotal;
    private Button btnFocus;
    private Button btnPesas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elegir_ejercicio);
        btnCardio = findViewById(R.id.btnCardio);
        btnAbs =findViewById(R.id.btnAbs);
        btnEjerTotal = findViewById(R.id.btnEjerTotal);
        btnFocus = findViewById(R.id.btnFocus);
        btnPesas = findViewById(R.id.btnPesas);

    }
    public void irCardio (View view){
        Intent intent = new Intent(elegirEjercicio.this,ActivityCardio.class);
        startActivity(intent);
    }
}
