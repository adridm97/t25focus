package com.example.adri.t25focus;

import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

/**
 * @author adri
 */
public class MainActivity extends AppCompatActivity {
    conexionSQLite conn = new conexionSQLite(this);

    private SQLiteDatabase db;
    private EditText txtUser;
    private EditText txtPwd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtUser = findViewById(R.id.edtTxtUsuari);
        txtPwd = findViewById(R.id.edtTxtPwd);



    }
            public void acceder(View view) {

                EditText a = findViewById(R.id.edtTxtUsuari);
                String user = a.getText().toString();
                EditText b = findViewById(R.id.edtTxtPwd);
                String pass = b.getText().toString();
                String password = conn.buscaPwd(user);

                if (pass.equals(password)){
                    Intent intent = new Intent(MainActivity.this, Calendari.class);
                    startActivity(intent);
                } else {
                    Toast t = Toast.makeText(MainActivity.this,"Usuario o contraseña incorrectos",Toast.LENGTH_SHORT);
                    t.show();
                }if (user.isEmpty()|| pass.isEmpty()){
                    Toast t = Toast.makeText(MainActivity.this,"No puedes dejar los campos vacios",Toast.LENGTH_SHORT);
                    t.show();
                }

            }


            public void registre(View view) {
                Intent intent = new Intent(MainActivity.this, RegistroUsuario.class);
                startActivity(intent);
            }

            public void cambiaCat(View view) {
                Locale catalan = new Locale("ca", "CA");
                Locale.setDefault(catalan);
                Configuration config = new Configuration();
                config.locale = catalan;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Intent cat = new Intent(getApplicationContext(), MainActivity.class);
                cat.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(cat);
            }

            public void cambiaEsp(View view) {
                Locale espanol = new Locale("es", "ES");
                Locale.setDefault(espanol);
                Configuration config = new Configuration();
                config.locale = espanol;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Intent es = new Intent(getApplicationContext(), MainActivity.class);
                es.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(es);
            }

            public void cambiaEng(View view) {
                Locale ingles = new Locale("en", "EN");
                Locale.setDefault(ingles);
                Configuration config = new Configuration();
                config.locale = ingles;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Intent eng = new Intent(getApplicationContext(), MainActivity.class);
                eng.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(eng);
            }

        }

