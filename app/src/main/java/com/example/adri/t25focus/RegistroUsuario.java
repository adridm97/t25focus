package com.example.adri.t25focus;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.adri.t25focus.usuarios.users;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class RegistroUsuario extends AppCompatActivity {
    conexionSQLite con = new conexionSQLite(this);
    ImageView img;
    Button btnFotoUsr;
    final static int cons = 0;

    private Button btnSubmit;
    private boolean validaPermisos() {
        if (Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
            return true;

        }
        if ((checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED)&&
                (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)){
            return true;
        }
        if ((shouldShowRequestPermissionRationale(CAMERA))||(shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))){
            cargarRecomendacion();
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);

        }
        return false;
    }
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrousuario);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnFotoUsr = findViewById(R.id.btnTomaFoto);
        img = findViewById(R.id.imgUser);
        if (validaPermisos()){
            btnFotoUsr.setEnabled(true);
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==100){
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
            &&grantResults[1]== PackageManager.PERMISSION_GRANTED){
                btnFotoUsr.setEnabled(true);
            }else {
                solicitarPermisosManual();
            }
        }
    }

    private void solicitarPermisosManual() {
        final CharSequence[]opciones ={"si","no"};
        final AlertDialog.Builder alertOpciones = new AlertDialog.Builder(RegistroUsuario.this);
        alertOpciones.setTitle("Desea configurar manualmente los permisos?");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (opciones[i].equals("si")){
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package",getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(),"Los permisos no han sido aceptados",Toast.LENGTH_SHORT);
                    dialog.dismiss();
                }
            }
        });{


        }

    }

    private void cargarRecomendacion() {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(RegistroUsuario.this);
        dialogo.setTitle("Permisos desactivados");
        dialogo.setMessage("Debe aceptar los permisos para que pueda utilizar la camara en esta app");
        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
            }
        });
        dialogo.show();
    }

    public void tomarFoto (View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,cons);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = (Bitmap)data.getExtras().get("data");
        img.setImageBitmap(bitmap);
    }

    public void onClick (View view) {

            EditText escribeNombre = findViewById(R.id.txtEscribeNombre);
            EditText escribeUsuario = findViewById(R.id.txtEscribeUsuario);
            EditText escribeContraseña = findViewById(R.id.txtEscribeContraseña);
            String nombre = escribeNombre.getText().toString();
            String usuario = escribeUsuario.getText().toString();
            String contraseña = escribeContraseña.getText().toString();
            users u = new users();
            u.setNombre(nombre);
            u.setNombreUsuario(usuario);
            u.setContraseña(contraseña);
            con.insertaUsuario(u);



        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}
