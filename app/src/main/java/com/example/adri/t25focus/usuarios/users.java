package com.example.adri.t25focus.usuarios;

public class users {
    int id;
    private String nombreUsuario;
    private String nombre;
    private String contraseña;



    public int getID(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }
    public String getNombreUsuario() {
        return this.nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContraseña() {
        return this.contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
}

