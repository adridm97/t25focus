package com.example.adri.t25focus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.adri.t25focus.usuarios.users;

public class conexionSQLite extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="bd_usuarios";
    public static final String NOMBRE_TABLA = "usuarios";
    public static final String CAMPO_ID = "id ";
    public static final String CAMPO_NOMBREUSUARIO = "nombreUsuario";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_CONTRASEÑA = "contraseña";
    SQLiteDatabase db;
    private static final String CREAR_TABLA = "CREATE TABLE usuarios (id int  primary key not null, nombreUsuario text not null, nombre text not null, contraseña text not null);";


    public conexionSQLite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREAR_TABLA);
        this.db=db;
    }

    public void insertaUsuario(users u){
        db = this.getWritableDatabase();
        ContentValues valores = new ContentValues();
        String query = "select * from usuarios";
        Cursor c = db.rawQuery(query,null);
        int count = c.getCount();

        valores.put(CAMPO_ID, count);
        valores.put(CAMPO_NOMBREUSUARIO, u.getNombreUsuario());
        valores.put(CAMPO_NOMBRE,u.getNombre());
        valores.put(CAMPO_CONTRASEÑA, u.getContraseña());
        db.insert(NOMBRE_TABLA, null,valores);
        db.close();
    }

    public String buscaPwd (String nombreUsuario){
        db = this.getReadableDatabase();
        String query = "SELECT nombreUsuario, contraseña from " +NOMBRE_TABLA;
        Cursor c = db.rawQuery(query,null);
        String a,b;
        b = "no encontrado";
        if (c.moveToFirst()){
            do{
                a = c.getString(0);
                if (a.equals(nombreUsuario)){
                    b = c.getString(1);
                    break;
                }
            }
            while (c.moveToNext());
        }
        return b;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = ("DROP TABLE if exists "+NOMBRE_TABLA);
        db.execSQL(query,null);
        this.onCreate(db);
    }
}
