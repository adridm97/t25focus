package com.example.adri.t25focus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Calendari extends AppCompatActivity {
    private Button btnStartWorkout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendari);
        btnStartWorkout = findViewById(R.id.btnStartWorkout);
    }
    public void empiezaEntreno(View v){
        Intent intent = new Intent(this, elegirEjercicio.class);
        startActivity(intent);
    }
}
