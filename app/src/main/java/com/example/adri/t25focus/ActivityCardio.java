package com.example.adri.t25focus;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.MediaController;
import android.widget.RadioButton;
import android.widget.VideoView;

public class ActivityCardio extends AppCompatActivity {
    RadioButton rCompleto;
    RadioButton rIncompleto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardio);
        rCompleto = findViewById(R.id.rbtnCompletado);
        rIncompleto = findViewById(R.id.rbtnNoCompletado);



    }
    public void cargaVideo (View view){
        VideoView videoCardio = findViewById(R.id.videoCardio);
        Uri path = Uri.parse("android.resource://"+getPackageName()+"/"
                + R.raw.video_cardio);
        MediaController mc = new MediaController(this);
        videoCardio.setMediaController(mc);
        videoCardio.setVideoURI(path);
        videoCardio.start();
    }
}
